package com.creditmanagement.cmtservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CmtServiceApplication {

  public static void main(String[] args) {
    SpringApplication.run(CmtServiceApplication.class, args);
  }

}
