package com.creditmanagement.cmtservice.ping;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping
public class Ping {

  private static final Logger logger = LoggerFactory.getLogger(Ping.class);


  @GetMapping("/ping")
  public ResponseEntity pingApi(){
    logger.info("Success hit at ping");
    return ResponseEntity.ok("UP");
  }
}
