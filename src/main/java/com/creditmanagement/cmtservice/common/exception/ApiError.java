package com.creditmanagement.cmtservice.common.exception;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
class ApiError {

  private int status;
  private String message;

  ApiError(int status) { this.status = status; }

  ApiError(int status, Throwable ex) {
    this.status = status;
    this.message = ex.getLocalizedMessage();
  }

  ApiError(int status, String message) {
    this.status = status;
    this.message = message;
  }

}
