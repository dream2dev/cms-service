package com.creditmanagement.cmtservice.common.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import lombok.extern.slf4j.Slf4j;

@ControllerAdvice
@Slf4j
public class RestExceptionHandler {

  //403 Bad Request
  @ExceptionHandler(MethodArgumentNotValidException.class)
  public ResponseEntity<ApiError> methodArgsNotValidExceptions(MethodArgumentNotValidException ex) {
    StringBuilder sb = new StringBuilder();
    ex.getBindingResult().getAllErrors().forEach(error -> sb.append("In field ")
        .append(((FieldError) error).getField()).append(", ").append(error.getDefaultMessage()).append(".  "));
    log.error(String.format("Method arguments are not valid. %s", sb.toString()));
    ApiError apiError = new ApiError(HttpStatus.BAD_REQUEST.value(), sb.toString());
    return new ResponseEntity<>(apiError, HttpStatus.BAD_REQUEST);
  }

  @ExceptionHandler(HttpMessageNotReadableException.class)
  public ResponseEntity<ApiError> httpMessageNotReadable(HttpMessageNotReadableException ex) {
    log.error("Unable to read " + ex.getLocalizedMessage());
    ApiError apiError = new ApiError(HttpStatus.BAD_REQUEST.value(), "Unable to read JSON request");
    return new ResponseEntity<>(apiError, HttpStatus.BAD_REQUEST);
  }


  // 500 - internal server error
  @ExceptionHandler(Exception.class)
  public ResponseEntity<ApiError> exceptionHandler(Exception ex) {
    log.error(ex.getMessage());
    ApiError apiError = new ApiError(HttpStatus.INTERNAL_SERVER_ERROR.value(),
        "Something went wrong. Please contact System Admin");
    return new ResponseEntity<>(apiError, HttpStatus.INTERNAL_SERVER_ERROR);
  }


}
